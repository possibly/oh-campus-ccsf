# Open Hatch - CCSF Campus Website#

### About ###

[Open Hatch](https://openhatch.org/) is a "is a non-profit dedicated to matching prospective free software contributors with communities, tools, and education." For the third year in a row, in collaboration with the members of the Coders Club, OpenHatch has come to City College of San Francisco for their "[Open Hatch Comes to Campus](http://campus.openhatch.org/)" event.

The design and template was created by OpenHatch here. I made significant revisions to target the audience at my college, CCSF. 

### Link to original repo ###

This repo serves as a link to the GitHub repo where the code is maintained and shared among the contributors.

https://github.com/possibly/oh-campus-ccsf